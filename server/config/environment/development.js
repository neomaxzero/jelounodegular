'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/jelouangularfst3-dev'
  },

  seedDB: false
  
};
