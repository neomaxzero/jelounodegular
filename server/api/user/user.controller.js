'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

// filesystem for imgupload
var fs = require('fs');

// graphicsmagick for thumbnails
var gm = require('gm');
var imageMagick = gm.subClass({ imageMagick: true });

var validationError = function(res, err) {
  return res.json(422, err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function(req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if(err) return res.send(500, err);
    res.json(200, users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.role = 'user';
  newUser.save(function(err, user) {
    if (err) return validationError(res, err);
    var token = jwt.sign({_id: user._id }, config.secrets.session, { expiresInMinutes: 60*5 });
    res.json({ token: token });
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(401);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err, user) {
    if(err) return res.send(500, err);
    return res.send(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return validationError(res, err);
        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};

/**
 * Change a users profile
 */
exports.changeProfile = function(req, res, next) {
  var userId = req.user._id;
  //obtener toda la info
  var name = String(req.body.name);
  var lastname = String(req.body.lastname);
  var birthday = String(req.body.birthday);
  var country = String(req.body.country);
  var language = String(req.body.language);

  User.findById(userId, function (err, user) {
    user.name = name;
    user.lastname = lastname;
    user.birthday = birthday;
    user.country = country;
    user.language = language;
    user.save(function(err) {
      if (err) return validationError(res, err);
      res.send(200);
    });
  });
};

/**
 * Get my info
 */
exports.me = function(req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, function(err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.json(401);
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function(req, res, next) {
  res.redirect('/');
};

/**
 * Profile image upload
 */
exports.imgUpload = function(req, res) {      
  // We are able to access req.files.file thanks to 
    // the multiparty middleware
    var file = req.files.file;
    console.log(file.name);
    console.log(file.type);

    // get file extension
    var extension = file.name.split('.').pop();

    // get req.file location
    var filePath = ""+req.files.file.path;
    // date for the img name
    var dateNow = new Date().getTime();

    // new relative path for profile and thumb image
    var uploadPath = "assets/upload/profile/" + req.user._id + "-" + dateNow + '.' + extension;
    var thumbPath = "assets/upload/profile/thumb/" + req.user._id + "-" + dateNow + '.' + extension;
    
    // basepath for the image
    var basePath = process.cwd() + "/client/";
    
    // complete path for profile and thumb
    var newPath = basePath + uploadPath;
    var newThumb = basePath + thumbPath;
    //console.log(newPath);

    // read the req temp file
    fs.readFile(filePath, function(err, data) {
      if (err) {
        console.log('[error] readfile');
        res.send(err);
        return;
      }

      // write the temp file to the public folder
      fs.writeFile(newPath, data, function (err) {
        if (!err) {

          // generate thumbnail
          imageMagick(newPath)
            .noProfile()
            .gravity('Center')
            .thumb('100', '100', newThumb, 90, function (err) {
              
              if (err) {
                console.log('[error] Thumb Generation');
                console.log(err);
              }

              // delete old profile and thumb img
              User.findById(req.user._id, function (err, user) {

                // delete previous img
                if (user.profileImg) {
                  var oldPath = basePath + user.profileImg;
                  fs.unlink(oldPath, function(err) {
                    if(err){
                      console.log(err);
                      console.log('[error] Img unlink');
                      res.send(err);
                    }
                  });
                }

                // delete previous thumb img
                if (user.profileThumb) {
                  var oldThumbPath = basePath + user.profileThumb;
                  fs.unlink(oldThumbPath, function(err) {
                    if(err){
                      console.log(err);
                      console.log('[error] Thumb unlink');
                      res.send(err);
                    }
                  });
                }

                // update the user.model
                user.profileThumb = thumbPath;
                user.profileImg = uploadPath;
                user.save(function(err) {
                
                  if (err) {

                    fs.unlink(filePath, function() {
                      if (err) throw err;
                    });

                    return validationError(res, err);
                      }
                });
              });

              console.log('[no error] :)');

              // send the paths
              res.send({
                  uploaded: true,
                  uploadDir: uploadPath,
                  uploadThumb: thumbPath,
                });
              
          });
          
        } else {
          console.log('[error] writefile');
          res.send(err);
        }
      });

      fs.unlink(filePath, function() {
        if (err) throw err;
      });

    });
};
