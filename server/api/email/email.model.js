'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EmailSchema = new Schema({
  from: {
  	type:Schema.Types.ObjectId,
  	ref:'User'},
  to: {
  	type:Schema.Types.ObjectId,
  	ref:'User'},
  date:{type: Date, default: Date.now},
  content:String,
  active: Boolean
});


EmailSchema.statics = {
	loadRecent:function(myself,cb){
		//console.log(myself);
		this.find({})
			.populate({path:'from'})
			.sort('-date')
			.where('to').equals(myself)
			.limit(10)
			.exec(cb);
	}
}
module.exports = mongoose.model('Email', EmailSchema);