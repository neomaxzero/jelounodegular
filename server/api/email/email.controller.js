'use strict';

var _ = require('lodash');
var Email = require('./email.model');

// Get list of emails
exports.index = function(req, res) {
  //console.log(req.user);
  var to = req.user._id;
  Email.loadRecent(to,function  (err,emails) {
      if (err) {return handleError(res,err); } 
      return res.json(200,emails)
    });
 
};

// Get a single email
exports.show = function(req, res) {
  Email.findById(req.params.id, function (err, email) {
    if(err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    return res.json(email);
  });
};

// Creates a new email in the DB.
exports.create = function(req, res) {
  var someFail = false;
  //For each destination you should save the message
 // console.log(req.body);
  for (var i = 0; i < req.body.to.length; i++) {
    var msg = new Email({from:req.body.from,to:req.body.to[i],content:req.body.content});
    console.log(msg);
    msg.save(function  (err,msg) {
      if(err){
        someFail= true;
      }

    })
  };
  // Email.create(req.body, function(err, email) {
  //   if(err) { return handleError(res, err); }
  //   return res.json(201, email);
  // });

  if (someFail) {
    return handleError(res,err);
  } else{
    return res.send(204);
  };
  
};

// Updates an existing email in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Email.findById(req.params.id, function (err, email) {
    if (err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    var updated = _.merge(email, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, email);
    });
  });
};

// Deletes a email from the DB.
exports.destroy = function(req, res) {
  Email.findById(req.params.id, function (err, email) {
    if(err) { return handleError(res, err); }
    if(!email) { return res.send(404); }
    email.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}