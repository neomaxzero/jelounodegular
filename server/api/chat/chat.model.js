'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ChatSchema = new Schema({
  msg: String,
  date:{type: Date, default: Date.now},
  author:{
  	type:Schema.Types.ObjectId,
  	ref:'User'
  }
});

ChatSchema.statics = {
	loadRecent:function(cb){
		this.find({})
			.populate({path:'author',select:'username'})
			.sort('-date')
			.limit(10)
			.exec(cb);

	},

	loadMore:function (date,cb) {
		this.find({})
			.populate({path:'author',select:'username'})
			.where('date').lt(date)
			.sort('-date')
			.limit(20)
			.exec(cb);

	}
};

module.exports = mongoose.model('Chat', ChatSchema);