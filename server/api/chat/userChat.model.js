'use strict';
//<UserBag> is the list of users connected.
var userBag= [];
var _ = require('lodash');
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var User = require('../user/user.model')

//Clase Eventos
var events = require('events');

//Emisor de Eventos
var eventEmitter = new events.EventEmitter();

//Cuando se agrega un usuario al Chat se emite el usuario salvado al resto de usuarios Conectados.
exports.register = function (userR,cb) {
	register(userR, function (data) {
		eventEmitter.emit('userChat:save');
		cb(data);
	});
	
}

// exports.newEvent = function (event,cb){
// 		eventEmitter.on(event,function 	 () {
			
// 			cb()
// 		})
// }

//Desconecta el usuario
exports.disconnect = function  (id,cb) {
	
		//Primero cargo el usuario
		var disconnected = _.find(userBag, {id: id});
		//Luego lo saco del arreglo
		disconnectInternal(id);
		cb(disconnected);
		
}

//Registra el evento de que cuando se conecta un usuario, debe actualizar la lista
exports.connectPost =function (event,cb) {
	
	eventEmitter.on('userChat:'+event,function(){
		//console.info('Se escucho el evento'); 
		cb(userBag[userBag.length-1]);
	});
	
	//console.info('Debug: UserChat.model.js EMITIO: metodo connectPost');
		
}


exports.logoutFromPost = function  (id,callbackToController) {		
	eventEmitter.emit('userChat:remove',id);
	
	callbackToController();
}

exports.logout = function(event,cb) {
	eventEmitter.on('userChat:'+event,function  (id) {
		
		//Primero cargo el usuario
		var disconnected = _.find(userBag, {_id: id});
		
		//Luego lo saco del arreglo
		


		disconnectInternal(id);
		
		cb(disconnected);
	})

};

//Saca el usuario de la lista y devuelve el usuario que fue expulsado
function disconnectInternal (id) {
	var oldItem = _.find(userBag, {id: id});
    		var index = userBag.indexOf(oldItem);	
    		if (oldItem) {
            userBag.splice(index, 1);
           	}
           	return (oldItem);

}
function register (id, cb) {
	var nuevoUser;

	User.findById(id,'-salt -hashedPassword',function (err,user) {
		 if (!err) {
		 	
		 	nuevoUser = user;
		 	//console.info(user);
		 	//nuevoUser._id = id;	
		 	var oldItem = _.find(userBag, {id: nuevoUser.id});
    		var index = userBag.indexOf(oldItem);
    		if (oldItem) {
            userBag.splice(index, 1, nuevoUser);
           	} else {
            userBag.push(nuevoUser);
         	 }
   
		 }
		 cb(userBag);
	})

	
	//Termina de registrar emite el evento
	//return userBag;
}
function getUB(){
	return userBag;
}


