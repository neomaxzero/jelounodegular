'use strict';

var express = require('express');
var controller = require('./chat.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/',auth.isAuthenticated(), controller.index);

router.get('/users',auth.isAuthenticated(), controller.getUsers);
//router.get('/:id', controller.show);
//router.post('/', controller.create);
router.post('/disconnect',auth.isAuthenticated(), controller.disconnect);
router.post('/', auth.isAuthenticated(), controller.create);
router.post('/moreMessages',auth.isAuthenticated(), controller.moreMessages);
//router.put('/:id', controller.update);
//router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);


module.exports = router;