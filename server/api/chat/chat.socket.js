/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Chat = require('./chat.model');
var userChat= require('./userChat.model')
//var Chat = require('./')

//VARIABLE DE USUARIOS


exports.register = function(socket) {
  Chat.schema.post('save', function (doc) {
    onSave(socket, doc);
  });

  Chat.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });

  userChat.connectPost('save',function (user) {

      socket.emit('userChat:save',user);
    });

  userChat.logout('remove',function  (user) {
     socket.broadcast.emit('userChat:remove',user);
   });

   socket.on('isWriting',function  (user) {
        
       socket.broadcast.emit('isWriting',user);
     });

   socket.on('stoppedWriting',function  (user) {
       
       socket.broadcast.emit('stoppedWriting',user);
     });
 }


 exports.disconnect = function  (socket) {
  //Extraigo el id
  var User_id = socket.decoded_token._id;
  //console.info('Desconectandose: '+User_id);
   userChat.disconnect(User_id,function  (user) {
    //console.info('Desconectandose_Callback: '+user);
     socket.broadcast.emit('userChat:remove',user);
   })
 }


function onSave(socket, doc, cb) {
	Chat.populate(doc,{path:'author',select:'username'} , function(err, msg) {
  socket.emit('chat:save', msg);
	});
}

function onRemove(socket, doc, cb) {
  socket.emit('chat:remove', doc);
}

