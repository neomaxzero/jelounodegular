'use strict';

var _ = require('lodash');
var Chat = require('./chat.model');
var userChat= require('./userChat.model')

//var usersConnected=[];
//
// Get list of chats
exports.index = function(req, res) {

    //CARGAR USUARIOS CONECTADOS??
  //updateUsers(req,res);

  Chat.loadRecent(function (err, chats) {
    if(err) { return handleError(res, err); }
    return res.json(200, chats);
  });
};

// Get a single chat
exports.show = function(req, res) {Chat.findById(req.params.id, function (err, chat) {
    if(err) { return handleError(res, err); }
    if(!chat) { return res.send(404); }
    return res.json(chat);
  });
};

// Creates a new chat in the DB.
exports.create = function(req, res) {
  delete req.body.date;
  var msg= new Chat(_.merge({author:req.user._id},req.body));
  msg.save(function  (err,msg) {
    if (err) {return handleError(res,err);}
    return res.json(201,msg);
  })
};

// Updates an existing chat in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Chat.findById(req.params.id, function (err, chat) {
    if (err) { return handleError(res, err); }
    if(!chat) { return res.send(404); }
    var updated = _.merge(chat, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, chat);
    });
  });
};

// Deletes a chat from the DB.
exports.destroy = function(req, res) {
  Chat.findById(req.params.id, function (err, chat) {
    if(err) { return handleError(res, err); }
    if(!chat) { return res.send(404); }
    chat.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};
  
exports.getUsers =function  (req,res) {
  //return updateUsers(req,res);
 userChat.register(req.user._id,function(lista) {
  //  console.info("[listalistalista]" + lista);
    return res.json(201,lista);
 });
  
};

exports.moreMessages = function(req,res) {
  
   Chat.loadMore(req.body.messageReference.date,function  ( err,msgs) {
    if(err) { return handleError(res, err); }
    //console.log(msgs);
    return res.json(200, msgs);
   });
  
};

exports.disconnect = function  (req,res) {
  userChat.logoutFromPost(req.user._id,function  () {
    return res.send(204);
  })
} 


function handleError(res, err) {
  return res.send(500, err);
}



//Soy un pelotudo...no editar esta funcion si no se USA>...derp
function updateUsers (req,res) {
 // var msg= new Chat(_.merge({author:req.user._id},req.body));
  var Vuser = new {dataUser : req.user._id, no: 'Buenisimo'}
  usersConnected.push(Vuser);
  return res.json(200,usersConnected);
}

