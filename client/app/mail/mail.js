'use strict';

angular.module('jelouAngularFsT3App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('mail', {
        url: '/mail',
        templateUrl: 'app/mail/mail.html',
        controller: 'MailCtrl'
      });
  });