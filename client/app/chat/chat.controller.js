'use strict';

angular.module('jelouAngularFsT3App')
  .controller('ChatCtrl', function ($scope,$http,$anchorScroll,socket,Auth) {
  //Variables Locales

  var hideBtnMoreMessages= false;
  var infoKeySent = false;
  var timer;
  //var statusMessage =false;
  //Variables de angular
 // $scope.chatMSG = [];
  $scope.hideBtnMoreMessages = hideBtnMoreMessages;
  $scope.statusMessage = false;
  	//var firstMessage;
    //Cuando hago un POST llama al metodo del servidor que corresponde con la ruta
	$scope.getCurrentUser = Auth.getCurrentUser;

	$http.get('/api/chats').success(function(chat){
		
		
		$scope.chatMSG = chat;
	
		socket.syncUpdates('chat',$scope.chatMSG,function  (event,sasa,chatMSG) {
				      scrolltoBottom();
			
		});
	


		// Searching for the connected user's list 
		$http.get('/api/chats/users').success(function  (users) {	
			$scope.usersConnected = users;
			socket.syncUpdates('userChat',$scope.usersConnected);
			scrolltoBottom();
		});
		
	});
	
    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('chat');
    });
 
    // Use our rest api to post a new comment --POST DEL MENSAJE
    $scope.send = function() {
      $http.post('/api/chats', { msg:$scope.msg });
      $scope.msg = '';
      
    };

      //Cuando esta escribiendo, el usuario que esta escribiendo
     socket.socket.on('isWriting',function  (user) {
      if ($scope.statusMessage) {
           $scope.statusMsgInfo = 'Someone is Writing';
      } 

        else{
        $scope.statusMessage = true;
         $scope.statusMsgInfo = user.name +' is writing';
       };
         
         
     });

     socket.socket.on('stoppedWriting',function    (  user) {
        $scope.statusMessage = false;
     })


    var resetTimer = function    () {
         timer =  setTimeout(function () {
            
            socket.socket.emit('stoppedWriting',Auth.getCurrentUser());
            infoKeySent =false;
        },3000);
    };


      //FUNCION SIMPLIFICADA DE LOREAN
      // $scope.writing = function    () {
      //     socket.socket.emit('isWriting', Auth.getCurrentUser());

      // }



    $scope.writing = function    () {
      if (!infoKeySent) {
        socket.socket.emit('isWriting',Auth.getCurrentUser());
        infoKeySent = true;
       // console.log( Auth.getCurrentUser());
        resetTimer();
        

      }else{
          clearTimeout(timer);
          resetTimer();
      };
    }


    $scope.moreMessages = function  () {
    		      
              //ordenar los mjes por fecha, asi puedo obtener el mensaje más antiguo
              var msgs = $scope.chatMSG;
              msgs.sort(function sort(a,b){return new Date(a.date).getTime() - new Date(b.date).getTime()});
    		
              $http.post('/api/chats/moreMessages',{messageReference:msgs[0]})
      				.success(function 	 (messages) {
                  //console.log(messages);
                  //Si concatenaba los mensajes, angular hacia una nueva referencia de memoria, y no funcionaba.
                  // si agregamos los mensajes uno a uno evitamos que se actualize dicha referencia.
                  if (messages.length>0) {
                    for (var i = messages.length - 1; i >= 0; i--) {
                      $scope.chatMSG.push(messages[i]);
                    };
                     

                  }else{
                        console.log('entro');
                      $scope.hideBtnMoreMessages = true;
                  }

                  

                  
      				});

    };
    //Scroll Chat to bottom
    var scrolltoBottom= function () {
    	$('#chatCanvas').stop().animate({
			  scrollTop: $("#chatCanvas")[0].scrollHeight
			}, 800);
    }


    //Capitalize first letter
    var capitalizeFirstLetter= function  (word) {
    	return word.charAt(0).toUpperCase()+word.slice(1);
    }
    

  });
