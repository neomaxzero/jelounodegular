'use strict';

angular.module('jelouAngularFsT3App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('publicProfile', {
        url: '/profile/:id',
        templateUrl: 'app/publicProfile/publicProfile.html',
        controller: 'PublicProfileCtrl'
      });
  });