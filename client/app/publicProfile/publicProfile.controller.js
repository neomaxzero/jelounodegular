'use strict';

angular.module('jelouAngularFsT3App')
  .controller('PublicProfileCtrl', function ($scope, Auth, $stateParams) {
    var id = $stateParams.id;
    $scope.profile = {};
    $scope.profile = Auth.getUserProfile(id);
    console.log($scope.profile);
  });