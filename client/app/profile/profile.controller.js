'use strict';

angular.module('jelouAngularFsT3App')
  .controller('ProfileCtrl', function ($scope, $filter, Auth, $upload) {
  	$scope.formActive = false;
    $scope.editProf = false;
    $scope.uploading = false;
  	$scope.profile = {};
  	$scope.profile = Auth.getCurrentProfile();

  	$scope.activateForm = function () {
  		$scope.formActive = true;
  		$scope.editedProfile = $scope.profile;
  		$scope.editedProfile.birthday = new Date($scope.editedProfile.birthday);
  	};

  	$scope.cancelChanges = function () {
  		$scope.formActive = false;
  		$scope.editedProfile = {};
  	};

  	$scope.submitChanges = function () {
  		Auth.editProfile($scope.editedProfile)
  		.then(function() {
  			$scope.cancelChanges();
  			$scope.profile = Auth.getCurrentProfile();
  		});
  	};

    $scope.progress = 0;
    $scope.showProgress = false;

    $scope.$watch('file', function () {
      $scope.upload($scope.file);
    });

    $scope.upload = function (files) {

      
      if (files && files.length) {
        
        // init progress
        $scope.progress = 0;
        $scope.showProgress = true;
        $scope.uploading = true;

        for (var i = 0; i < files.length; i++) {
          
          var arch = files[i];
          
          $upload.upload(
          {
            url: 'api/users/image',
            method: 'POST',
            headers: { 'Authorization': 'Bearer ' + Auth.getToken() },
            file: arch
          }
          ).progress(function (evt) {
            
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            $scope.progress = progressPercentage;

          }).success(function (data, status, headers, config) {
            
            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
            
            $scope.profile.profileImg = data.uploadDir;

            // meanwhile, no thumbnail
            Auth.setProfileThumb(data.uploadThumb);
            
            setTimeout(function(){
              $scope.$apply(function(){                
                $scope.progress = 0;
                $scope.showProgress = false;
                $scope.editProf = false;
                $scope.uploading = false;
              });
            },2000);
              
          });
        }
      }
    };


  });