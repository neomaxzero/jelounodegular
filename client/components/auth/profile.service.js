'use strict';

angular.module('jelouAngularFsT3App')
  .factory('Profile', function ($resource) {
    return $resource('/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      edit: {
      	method: 'PUT',
      	params: {
      		controller: 'profile'
      	}
      },
      get: {
        method: 'GET'
      }
	  });
  });
